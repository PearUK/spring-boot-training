//package com.example.demo.dao;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class AppConfig {
//
//  // Lấy giá trị config từ file application.properties
//  @Value("${spring.datasource.url}")
//  String mysqlUrl;
//  @Value("${spring.datasource.username}")
//  String myuserName;
//  @Value("${spring.datasource.password}")
//  String mypassWord;
//
//  @Bean
//  DatabaseConnector mysqlConfigure() {
//    DatabaseConnector mySqlConnector = new MySqlConnector();
//    // Set Url
//    System.out.println("Config Mysql Url: " + mysqlUrl);
//    mySqlConnector.setUrl(mysqlUrl);
//    // Set username, password, format, v.v...
//    System.out.println("Config UserName: " + myuserName);
//    mySqlConnector.setUserName(myuserName);
//    System.out.println("Config mypassword: " + mypassWord);
//    mySqlConnector.setPassWord(mypassWord);
//    return mySqlConnector;
//  }
//}
