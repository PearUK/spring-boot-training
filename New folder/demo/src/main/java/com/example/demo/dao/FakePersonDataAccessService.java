//package com.example.demo.dao;
//
//import com.example.demo.model.Person;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.UUID;
//import org.springframework.stereotype.Repository;
//
//@Repository("fakeDao")
//public class FakePersonDataAccessService implements PersonDao {
//
//  private static List<Person> DB = new ArrayList<>();
//
//  @Override
//  public int insertPerson(UUID i, Person person) {
//    DB.add(new Person(i, person.getName()));
//    return 1;
//  }
//}
