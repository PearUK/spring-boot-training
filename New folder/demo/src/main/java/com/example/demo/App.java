package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class App {
//public class SpringBootJdbcTemplateSqlServerApplication implements CommandLineRunner {
//
//  @Autowired
//  private JdbcTemplate jdbcTemplate;

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
//  @Override
//  public void run(String... args) throws Exception {
//    String sql = "SELECT * FROM customers";
//    List<Customer> customers = jdbcTemplate.query(sql,
//        BeanPropertyRowMapper.newInstance(Customer.class));
//
//    customers.forEach(System.out::println);
//  }

}
