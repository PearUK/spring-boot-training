package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Controller
//public class WebController {
//
//    @GetMapping("/profile")
//    public String profile(Model model) {
//        // Tạo ra thông tin
//        List<Info> profile = new ArrayList<>();
//        profile.add(new Info("fullname", "Lê Anh Quốc"));
//        profile.add(new Info("nickname", "PearUK"));
//        profile.add(new Info("gmail", "leanhquoc02@gmail.com"));
//        profile.add(new Info("facebook", "https://www.facebook.com/qqpoopqq/"));
//        profile.add(new Info("gitlab", "https://gitlab.com/PearUK"));
//
//        // Đưa thông tin vào Model
//        model.addAttribute("pearukProfile", profile);
//
//        // TRả về template profile.html
//        return "profile";
//    }
//}

@Controller
public class WebController {
    // Sử dụng tạm List để chứa danh sách công việc
    // Thay cho Database.
    // Chỉ dùng cách này khi DEMO ^^
    List<Todo> todoList = new CopyOnWriteArrayList<>();

    /*
        @RequestParam dùng để đánh dấu một biến là request param trong request gửi lên server.
        Nó sẽ gán dữ liệu của param-name tương ứng vào biến
     */
    @GetMapping("/listTodo")
    public String index(Model model, @RequestParam(value = "limit", required = false) Integer limit) {
        // Trả về đối tượng todoList.
        // Nếu người dùng gửi lên param limit thì trả về sublist của todoList
        model.addAttribute("todoList", limit != null ? todoList.subList(0, limit) : todoList);

        // Trả về template "listTodo.html"
        return "listTodo";
    }

    @GetMapping("/addTodo")
    public String addTodo(Model model) {
        // Thêm mới một đối tượng Todo vào model
        model.addAttribute("todo", new Todo());
        // Trả về template addTodo.html
        return "addTodo";
    }
    
}
