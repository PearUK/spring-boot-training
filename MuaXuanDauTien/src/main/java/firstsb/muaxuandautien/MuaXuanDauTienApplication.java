package firstsb.muaxuandautien;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MuaXuanDauTienApplication {

    public static void main(String[] args) {
        SpringApplication.run(MuaXuanDauTienApplication.class, args);
    }

}
