package firstsb.muaxuandautien.api;

import firstsb.muaxuandautien.model.Person;
import firstsb.muaxuandautien.service.PersonService;

public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    public void addPerson(Person person) {
        personService.addPerson(person);
    }
}
