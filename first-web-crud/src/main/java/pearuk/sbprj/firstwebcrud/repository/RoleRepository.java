package pearuk.sbprj.firstwebcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pearuk.sbprj.firstwebcrud.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
}
