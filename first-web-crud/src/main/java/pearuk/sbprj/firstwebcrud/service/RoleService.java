package pearuk.sbprj.firstwebcrud.service;

import pearuk.sbprj.firstwebcrud.model.Role;

import java.util.List;

public interface RoleService {
    List<Role> getAllRoles();

    void saveRole(Role Role);

    Role getRoleById(Long id);

    void deleteRoleById(Long id);
}
