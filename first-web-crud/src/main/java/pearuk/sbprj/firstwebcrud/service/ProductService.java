package pearuk.sbprj.firstwebcrud.service;

import pearuk.sbprj.firstwebcrud.model.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAllProducts();

    void saveProduct(Product product);

    Product getProductById(Long id);

    void deleteProductById(Long id);
}
