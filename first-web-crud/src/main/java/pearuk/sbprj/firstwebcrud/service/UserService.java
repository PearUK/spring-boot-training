package pearuk.sbprj.firstwebcrud.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import pearuk.sbprj.firstwebcrud.model.User;

import java.util.List;

public interface UserService extends UserDetailsService {
    List<User> getAllUsers();

    User saveUserRegis(User user);

    void saveUser(User user);

    User getUserById(Long id);

    void deleteUserById(Long id);
}
