package pearuk.sbprj.firstwebcrud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pearuk.sbprj.firstwebcrud.model.Role;
import pearuk.sbprj.firstwebcrud.repository.RoleRepository;

import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository RoleRepository;

    @Override
    public List<Role> getAllRoles() {
        return RoleRepository.findAll();
    }

    @Override
    public void saveRole(Role role) {
        this.RoleRepository.save(role);
    }

    @Override
    public Role getRoleById(Long id) {
        Optional<Role> optional = RoleRepository.findById(id);
        Role role = null;
        if (optional.isPresent()) {
            role = optional.get();
        } else {
            throw new RuntimeException("Role not found for id :: " + id);
        }
        return role;
    }

    @Override
    public void deleteRoleById(Long id) {
        this.RoleRepository.deleteById(id);
    }

}
