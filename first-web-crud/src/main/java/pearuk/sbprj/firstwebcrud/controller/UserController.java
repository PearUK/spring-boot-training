package pearuk.sbprj.firstwebcrud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pearuk.sbprj.firstwebcrud.model.User;
import pearuk.sbprj.firstwebcrud.service.RoleService;
import pearuk.sbprj.firstwebcrud.service.UserService;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @GetMapping("/tables/new-user-form")
    public String addNewUser(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        model.addAttribute("listRoles", roleService.getAllRoles());
        model.addAttribute("title", "Add User");
        return "pages/user_form";
    }

    @PostMapping("/tables/save-user")
    public String saveUser(@ModelAttribute("product") User user) {
        userService.saveUser(user);
        return "redirect:/tables";
    }

    @GetMapping("/tables/update-user-form/{id}")
    public String updateUser(@PathVariable(value = "id") long id, Model model) {

        //get from service
        User user = userService.getUserById(id);

        //set as model attribute
        model.addAttribute("user", user);
        model.addAttribute("listRoles", roleService.getAllRoles());
        model.addAttribute("title", "Update User");
        return "pages/user_form";
    }

    @GetMapping("/tables/delete-user/{id}")
    public String deleteUser(@PathVariable(value = "id") Long id) {

        //call delete method
        this.userService.deleteUserById(id);
        return "redirect:/tables";
    }
}
