package pearuk.sbprj.firstwebcrud.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pearuk.sbprj.firstwebcrud.model.User;
import pearuk.sbprj.firstwebcrud.service.UserService;

@Controller
@RequestMapping("/sign-up")
@AllArgsConstructor
public class UserRegisterController {

    private UserService userService;

    @ModelAttribute("user")
    public User userRegistration() {
        return new User();
    }

    @GetMapping
    public String registrationForm() {
        return "pages/sign-up";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") User registration) {
        userService.saveUserRegis(registration);
        return "redirect:/sign-up?success";
    }
}
