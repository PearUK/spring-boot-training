package pearuk.sbprj.firstwebcrud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pearuk.sbprj.firstwebcrud.model.Product;
import pearuk.sbprj.firstwebcrud.service.ProductService;

@Controller
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/tables/new-product-form")
    public String newProductForm(Model model) {
        Product product = new Product();
        model.addAttribute("product", product);
        model.addAttribute("title", "Add Product");
        return "pages/product_form";
    }

    @PostMapping("/tables/save-product")
    public String saveProduct(@ModelAttribute("product") Product product) {
        productService.saveProduct(product);
        return "redirect:/tables";
    }

    @GetMapping("/tables/update-product-form/{id}")
    public String updateProductForm(@PathVariable(value = "id") long id, Model model) {

        //get from service
        Product product = productService.getProductById(id);

        //set as model attribute
        model.addAttribute("product", product);
        model.addAttribute("title", "Update Product");
        return "pages/product_form";
    }

    @GetMapping("/tables/delete-product/{id}")
    public String deleteProduct(@PathVariable(value = "id") Long id) {

        //call delete method
        this.productService.deleteProductById(id);
        return "redirect:/tables";
    }
}
