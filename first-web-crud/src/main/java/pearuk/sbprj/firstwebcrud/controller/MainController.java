package pearuk.sbprj.firstwebcrud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pearuk.sbprj.firstwebcrud.service.ProductService;
import pearuk.sbprj.firstwebcrud.service.UserService;

@Controller
public class MainController {

    @Autowired
    private ProductService productService;
    @Autowired
    private UserService userService;

    @GetMapping("/sign-in")
    public String login() {
        return "pages/sign-in";
    }

    //display list of product, user
    @GetMapping("/tables")
    public String viewHomePage(Model model) {
        model.addAttribute("listProducts", productService.getAllProducts());
        model.addAttribute("listUsers", userService.getAllUsers());
        return "pages/tables";
    }
}
